package com.conygre.spring.portfolio.repo;

import com.conygre.spring.portfolio.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, ObjectId> {
    public Trade findByTicker(String ticker);
    //public void updateStatus(ObjectId id, String status);
    public void deleteById(ObjectId id);
    
}