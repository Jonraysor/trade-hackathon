package com.conygre.spring.portfolio.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    private ObjectId id;
    private String ticker;
    private double price;
    private double quantity;
    private String state;
    private Double value;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Trade() {

    }

    public Trade(String ticker, double price, double quantity, String state) {
        this.ticker = ticker;
        this.price = price;
        this.quantity = quantity;
        this.state = state;
        this.value = price * quantity;
    }

    @Override
    public String toString() {
    
        return ticker + " " + price + " " +quantity + " value " + value;
    }
}